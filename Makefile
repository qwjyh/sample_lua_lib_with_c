main:
	gcc -Wall -Wextra -O2 -lm -llua -o lsf src/main.c
lib:
	gcc -Wall -Wextra -fPIC -shared -O2 -lm -llua -o lsf.so src/lsf.c
