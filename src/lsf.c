#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "lauxlib.h"
#include "lua.h"
#include "lualib.h"

#include "lsf.h"

static int l_gamma(lua_State *L) {
    double arg = luaL_checknumber(L, 1);

    double ans;
    ans = tgamma(arg);

    lua_pushnumber(L, ans);

    return 1;
}

static int l_id(lua_State *L) {
    double arg = luaL_checknumber(L, 1);

    lua_pushnumber(L, arg);

    return 1;
}

static const struct luaL_Reg lsf [] = {
    {"gamma", l_gamma},
    {"id", l_id},
    {NULL, NULL},
};

int luaopen_lsf(lua_State *L) {
    luaL_newlib(L, lsf);
    return 1;
}

