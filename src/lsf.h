#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "lauxlib.h"
#include "lua.h"
#include "lualib.h"

#ifndef RSF_LIB
#define RSF_LIB

static int l_gamma(lua_State *L);
static int l_id(lua_State *L);
int luaopen_lsf(lua_State *L);

#endif
