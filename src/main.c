#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "lauxlib.h"
#include "lua.h"
#include "lualib.h"

#include "./lsf.h"

void dumpStack(lua_State *L) {
  int i;
  int stackSize = lua_gettop(L);
  for (i = stackSize; i >= 1; i--) {
    int type = lua_type(L, i);
    printf("Stack[%2d-%10s]: ", i, lua_typename(L, type));

    switch (type) {
    case LUA_TNUMBER:
      printf("%f", lua_tonumber(L, i));
      break;
    case LUA_TBOOLEAN:
      if (lua_toboolean(L, i)) {
        printf("true");
      } else {
        printf("false");
      }
      break;
    case LUA_TSTRING:
      printf("%s", lua_tostring(L, i));
      break;
    case LUA_TNIL:
      break;
    default:
      printf("%s", lua_typename(L, type));
      break;
    }
    printf("\n");
  }
  printf("\n");
}

int l_add(lua_State* L) {
  double x = luaL_checknumber(L, 1);
  double y = luaL_checknumber(L, 2);

  double result = x + y;
  printf("calc result %4f", result);

  lua_pushnumber(L, result);
  return 1; // return number
}

int main(int argc, char** argv) {
  lua_State* L = luaL_newstate();
  luaL_openlibs(L);

  lua_register(L, "cadd", l_add);

  if (luaL_loadfile(L, "init.lua") || lua_pcall(L, 0, 0, 0)) {
    printf("failed to open init.lua\n");
    printf("error : %s\n", lua_tostring(L, -1));
    return 1;
  } 

  lua_close(L);
  return 0;
}
